#!/bin/sh

# enable stopping on error
set -e


# parameters

WORKDIR="workdir"


# logging setup

alias log_debug=true # disable output
#alias log_debug="echo [debug]"
alias log_info="echo [info]"
alias log_warn="echo [warning]"
alias log_err="echo [error]"


# function definitions

usage() {
	echo "multiTCP (mtcp) (c) 2016 by M-1"
	echo "connects to multiple TCP hosts and sends the same data to all of them"
	echo "Usage:"
	echo "    mtcp <what> [hostlist]"
	echo "      where <what> is:"
	echo "        connect <hostlist>    connects to the specified hosts"
	echo "        send                  sends data from stdin to all hosts at once"
	echo "        disconnect            disconnects from all connected hosts"
}

prepare_host_port() {
	# - create pipes (connection's input + output)
	if [ $# -ne 2 ]; then
		log_err "prepare_host_port: needs 2 parameters"
		exit
	fi

	host=$1
	port=$2

	# do the rest in a subshell to avoid changing the working directory globally
	(

	cd "$WORKDIR"

	name="$1_$2"
	pipename="$name.pipe_in"
	pipeoutname="$name.pipe_out"

	if [ -p "$pipename" ]; then
		log_warn "pipe for $host:$port already exists: $pipename"
		return
	fi

	log_debug "creating pipes for $name: $pipename, $pipeoutname"
	mkfifo "$pipename"
	mkfifo "$pipeoutname"

	)

}

connect_host_port() {
	# - connect to given host:port via nc (and save nc's PID)
	if [ $# -ne 2 ]; then
		log_err "connect_host_port: needs 2 parameters"
		exit
	fi

	host=$1
	port=$2

	log_debug "host: $host, port: $port"


	# do the rest in a subshell to avoid changing the working directory globally
	(

	cd "$WORKDIR"

	name="$1_$2"
	pipename="$name.pipe_in"
	pipeoutname="$name.pipe_out"
	reader_pid="$name.reader_pid"
	nc_pid="$name.nc_pid"

	# start nc in separate subshell (to keep it running in the background and cleanup after it finishes)
	(
		if [ -f "$nc_pid" ]; then
			log_warn "nc for $name already running (pid exists)"
			return
		fi


		log_info "connecting to $host:$port"
		# start nc, redirect its input/output from/to pipes
		nc -v "$host" "$port" < "$pipename" > "$pipeoutname" 2>&1 &
		#nc -v "$host" "$port" < "$pipename" > /dev/null 2>&1 &
		echo $! > "$nc_pid"
		log_debug "nc_pid: $!"

		# start the reader – whenever nc outputs something, print it (prepend the name for easy identification)
		sed "i\\
[out] $name: " "$pipeoutname" &
		echo $! > "$reader_pid"
		log_debug "reader_pid: $!"

		# if this shell gets terminated, close all processes (including nc) and clean up
		trap "log_debug \"TRAP - nc runnung\"; kill `cat $nc_pid`; rm \"$nc_pid\" \"$reader_pid\" \"$pipename\" \"$pipeoutname\"" EXIT

		log_debug "waiting for nc to finish"
		wait #`cat $nc_pid` # wait until nc finishes
		log_debug "nc finished"
		log_info "connection to $host:$port ended"

		# if this shell gets terminated, close all processes (excluding nc) and clean up
		trap "log_debug \"TRAP - nc not runnung\"; rm \"$nc_pid\" \"$reader_pid\" \"$pipename\" \"$pipeoutname\"" EXIT
	) &

	)

}

prepare_hostport() {
	# - split host:port into 2 parameters
	# - call prepare_host_port

	if [ $# -lt 1 ]; then
		log_err "prepare_hostport: needs 1 parameter"
		exit
	fi

	# split host:port to 2 variables
	IFS=":" read host port <<-END
		$1
	END

	prepare_host_port "$host" "$port"
}

connect_hostport() {
	# - split host:port into 2 parameters
	# - call connect_host_port

	if [ $# -lt 1 ]; then
		log_err "connect_hostport: needs 1 parameter"
		exit
	fi

	# split host:port to 2 variables
	IFS=":" read host port <<-END
		$1
	END

	connect_host_port "$host" "$port"
}

connect_multi() {
	for i in $@; do
		prepare_hostport "$i"
	done
	# - start dummy writer (and save the PID) to avoid closing nc because of the lack of input
	(
		cd "$WORKDIR"
		log_debug "starting dummy writer"
		trap "log_debug killing dummy writer; kill `cat dummy_writer.pid`; rm dummy_writer.pid" EXIT
		( sleep 604800 & echo $! > "dummy_writer.pid" ) | tee `ls *.pipe_in` > /dev/null
	) &

	for i in $@; do
		connect_hostport "$i"
	done
}

send() {
	# subshell
	(
	cd "$WORKDIR"

	# read from stdin, write the data to all open pipes
	tee `ls *.pipe_in` > /dev/null
	)
}

disconnect() {

	(
	cd "$WORKDIR"
	if ! ls *.nc_pid > /dev/null 2>&1; then
		log_warn "no open connections"
		return
	fi

	# kill the nc (for every open connection), the rest should be handled by trap-cleanup in connect_host_port
	kill `cat *.nc_pid`

	log_debug "killing dummy writer"
	kill "`cat dummy_writer.pid`"
	rm dummy_writer.pid
	)
}


# the program execution starts here

mkdir -p "$WORKDIR"

if [ $# -lt 1 ]; then
	usage
	exit 1
fi

what="$1"

case "$what" in
	connect)
		hostlist="$2"
		if [ ! -f "$hostlist" ]; then
			log_err "hostlist does not exist: $hostlist"
			exit 1
		fi
		connect_multi `cat $hostlist`
		;;
	send)
		send
		;;
	disconnect)
		disconnect
		;;
	*)
		log_err "unknown <what>"
		usage
		exit 1
		;;
esac

